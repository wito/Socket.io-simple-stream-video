/*var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var express = require('express');*/

var fs = require('fs');
var https = require('https');
var socket = require('socket.io');
var express = require('express');
var app = express();
var options = {
    hostname: 'pulsejs.io',
    key: fs.readFileSync('ssl/privkey.pem'),
    cert: fs.readFileSync('ssl/fullchain.pem')
};
var puerto = 9443;
var server = https.createServer(options,app);
var io = socket.listen(server);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/screen-share-host', function(req, res){
  res.sendFile(__dirname + '/index-screenshare.html');
});

app.use(express.static('public'));

app.get('/client', function(req, res){
  res.sendFile(__dirname + '/client.html');
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });

  socket.on('video', function(frame){
  	io.emit('client', frame); //send frame to all clients.
  });

});

server.listen(puerto, function(){
  console.log('listening on *:'+puerto);
});
